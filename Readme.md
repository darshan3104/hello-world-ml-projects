
# Hello ML world

This project is an implementation for creation and understanding the basics of models in ML


## Tech Stack

**Language:** Python 3.9

**Framework:** Tensorflow 2.x


## Understanding Patterns in simple data manually
![sample dataset](assets/image.png)


## Related Articles

1. [Google Article](https://developers.google.com/codelabs/tensorflow-1-helloworld)
2. [Loss Functions and Optimizer](https://medium.com/geekculture/loss-functions-and-optimizers-in-ml-models-b125871ff0dc)



